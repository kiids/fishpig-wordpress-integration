<?php

class Fishpig_Wordpress_Addon_Multisite_Model_System_Config_Source_Blog extends Varien_Object
{
	protected $_options = null;
	
	public function toOptionArray()
	{
	
		if (is_null($this->_options)) {
			$this->_options = array();
			
			try {
				$helper = Mage::helper('wordpress/database');
				$db = $helper->getReadAdapter();
			
				$select = $db->select()
					->from($helper->getTableName('blogs'), 'blog_id');
					
				$blogIds = $db->fetchCol($select);
				
				foreach($blogIds as $blogId) {
					if ($blogId === '1') {
						$table = $helper->getTablePrefix() . 'options';
					}
					else {
						$table = $helper->getTablePrefix() . $blogId . '_options';
					}
		
					$select = $db->select()
						->from($table, 'option_value')
						->where('option_name=?', 'blogname')
						->limit(1);
						
					$this->_options[] = array('value' => $blogId, 'label' => $blogId . ': ' . $db->fetchOne($select));	
				}
			}
			catch (Exception $e) {
				$this->_options = array(array(
					'value' => 1, 'label' => Mage::helper('wordpress')->__('WordPress Multisite not installed'),
				));
				
				Mage::helper('wordpress')->log($e->getMessage());
			}
		}
		
		return $this->_options;
	}
}